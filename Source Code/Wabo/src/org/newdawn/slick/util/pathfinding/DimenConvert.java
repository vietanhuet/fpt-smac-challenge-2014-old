package org.newdawn.slick.util.pathfinding;

import java.util.ArrayList;

import org.newdawn.pathexample.GameMap;
import org.newdawn.slick.util.pathfinding.Path.Step;

import com.fpt.robot.types.RobotMoveTargetPosition;

public class DimenConvert {
	/**
	 * convert x,y of map to dimension of original coordinate
	 * @deprecated
	 * @param x
	 * @param y
	 * @return
	 */
	public static int[] convert(int x, int y) {
		int[] ret = new int[2];
		ret[0] = -1;
		ret[1] = -1;
		int midleY = GameMap.WIDTH / 2;
		int midleX = GameMap.HEIGHT / 2;
		if (x <= midleY) {
			ret[1] = (midleY - x) * GameMap.SIZE_OF_BLOCK * -1;
		} else {
			ret[1] = (x - midleY) * GameMap.SIZE_OF_BLOCK;
		}

		if (y <= midleX) {
			ret[0] = (midleX - y) * GameMap.SIZE_OF_BLOCK * -1;
		} else {
			ret[0] = (y - midleX) * GameMap.SIZE_OF_BLOCK;
		}
		return ret;
	}

	/**
	 * chuyá»ƒn tá»« há»‡ toáº¡ Ä‘á»™ mÃ£ hoÃ¡ thÃ nh há»‡ toáº¡ Ä‘á»™ thá»±c 
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public static float[] convert2(int x, int y) {
		float[] ret = new float[2];
		ret[0] = -1;
		ret[1] = -1;
		int midleY = GameMap.WIDTH / 2;
		int midleX = GameMap.HEIGHT / 2;
		if (x <= midleY) {
			ret[1] = (midleY - x) * 0.25f * -1;
		} else {
			ret[1] = (x - midleY) * 0.25f;
		}

		if (y <= midleX) {
			ret[0] = (midleX - y) * 0.25f * -1;
		} else {
			ret[0] = (y - midleX) * 0.25f;
		}
		return ret;
	}
	/**
	 * chuyá»ƒn tá»« há»‡ toáº¡ Ä‘á»™ thá»±c thÃ nh há»‡ toáº¡ Ä‘á»™ mÃ£ hoÃ¡ 
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public static int[] convertInMap(int x, int y) {
		int[] ret = new int[2];
		ret[0] = -1;
		ret[1] = -1;
		int midleY = GameMap.WIDTH / 2;
		int midleX = GameMap.HEIGHT / 2;
		if (x >= 0) {
			ret[1] = x / GameMap.SIZE_OF_BLOCK + midleY;
		} else {
			ret[1] = midleY - Math.abs(x) / GameMap.SIZE_OF_BLOCK;
		}

		// calculate X
		if (y <= 0) {
			ret[0] = midleX - Math.abs(y) / GameMap.SIZE_OF_BLOCK;
		} else {
			ret[0] = midleX + y / GameMap.SIZE_OF_BLOCK;
		}
		return ret;
	}

	
	/**
	 * TÃ­nh toÃ¡n cÃ¡c bÆ°á»›c cáº§n Ä‘i dá»±a vÃ o cÃ¡c toáº¡ Ä‘á»™ di chuyá»ƒn tá»« Ä‘iá»ƒm Ä‘áº§u Ä‘áº¿n Ä‘iá»ƒm cuá»‘i mÃ  Ä‘Æ°á»£c sinh ra tá»« thuáº­t toÃ¡n sinh Ä‘Æ°á»�ng 
	 * @param arr
	 * @return
	 * @deprecated
	 */
	public static ArrayList<RobotMoveTargetPosition> calculateTargetPosition(
			ArrayList<Step> arr) {
		int size = arr.size();
		ArrayList<RobotMoveTargetPosition> targets = new ArrayList<RobotMoveTargetPosition>();
		for (int i = 0; i < size - 1; i++) {
			Step step1 = arr.get(i);
			Step step2 = arr.get(i + 1);
			float x = (step2.getY() - step1.getY()) * 0.25f;
			float y = (step2.getX() - step1.getX()) * 0.25f;
			float theta = 0.0f;

			RobotMoveTargetPosition move = new RobotMoveTargetPosition(x, y,
					theta);
			targets.add(move);
		}
		return targets;
	}

	/**
	 * tÃ­nh toÃ¡n cÃ¡c object RobotMoveTargetPosition cáº§n pháº£i Ä‘i Ä‘á»ƒ Ä‘áº¿n Ä‘Æ°á»£c Ä‘iá»ƒm Ä‘Ã­ch (toX,toY) tá»« fromX fromY vá»›i gÃ³c quay robot ban Ä‘áº§u lÃ  lAngle 
	 * @param fromX
	 * @param fromY
	 * @param toX
	 * @param toY
	 * @param lAngle
	 * @return
	 */
	public static  ArrayList<RobotMoveTargetPosition> convert2Target(float fromX,float fromY,float toX,float toY,float lAngle) {
		ArrayList<RobotMoveTargetPosition> targets = new ArrayList<RobotMoveTargetPosition>();
		float lastAngle = lAngle;
		float x = (toX - fromX);
		float y = (toY - fromY);
		float angle = (float) Math.atan2(y, x);
		float a = getAngle(angle, x, y);
		float theta = a - lastAngle;
		currentAngle = a;
		if (theta != 0.0f) {
			if (theta < -Math.PI) {
				theta = (float) (2 * Math.PI + theta);
			} else if (theta > Math.PI) {
				theta = (float) (theta - 2 * Math.PI);
			}
			RobotMoveTargetPosition rot = new RobotMoveTargetPosition(0, 0,
					theta);
			targets.add(rot);
		}

		float distance = (float) Math.sqrt(x * x + y * y);
		RobotMoveTargetPosition move = new RobotMoveTargetPosition(
				distance, 0, 0);
		targets.add(move);
		return targets;
	}
	
	/**
	 * tÃ­nh toÃ¡n cÃ¡c object RobotMoveTargetPosition cáº§n pháº£i Ä‘i Ä‘á»ƒ Ä‘áº¿n Ä‘Æ°á»£c Ä‘iá»ƒm Ä‘Ã­ch (toX,toY) tá»« fromX fromY vá»›i gÃ³c quay robot ban Ä‘áº§u lÃ  hÆ°á»›ng theo trá»¥c X cá»§a sÃ¢n 
	 * @param fromX
	 * @param fromY
	 * @param toX
	 * @param toY
	 * @param lAngle
	 * @return
	 */
	public static  ArrayList<RobotMoveTargetPosition> convert2Target(float fromX,float fromY,float toX,float toY) {
		ArrayList<RobotMoveTargetPosition> targets = new ArrayList<RobotMoveTargetPosition>();
		float lastAngle = 0;
		float x = (toX - fromX);
		float y = (toY - fromY);
		float angle = (float) Math.atan2(y, x);
		float a = getAngle(angle, x, y);
		float theta = a - lastAngle;
		currentAngle = a;
		if (theta != 0.0f) {
			if (theta < -Math.PI) {
				theta = (float) (2 * Math.PI + theta);
			} else if (theta > Math.PI) {
				theta = (float) (theta - 2 * Math.PI);
			}
			RobotMoveTargetPosition rot = new RobotMoveTargetPosition(0, 0,
					theta);
			targets.add(rot);
		}

		float distance = (float) Math.sqrt(x * x + y * y);
		RobotMoveTargetPosition move = new RobotMoveTargetPosition(
				distance, 0, 0);
		targets.add(move);
		return targets;
	}
	
	/**
	 * TÃ­nh toÃ¡n cÃ¡c bÆ°á»›c cáº§n Ä‘i dá»±a vÃ o cÃ¡c toáº¡ Ä‘á»™ di chuyá»ƒn tá»« Ä‘iá»ƒm Ä‘áº§u Ä‘áº¿n Ä‘iá»ƒm cuá»‘i mÃ  Ä‘Æ°á»£c sinh ra tá»« thuáº­t toÃ¡n sinh Ä‘Æ°á»�ng 
	 * @param arr
	 * @return
	 */
	public static ArrayList<RobotMoveTargetPosition> calculateTargetPosition2(
			ArrayList<Step> arr) {
		int size = arr.size();
		ArrayList<RobotMoveTargetPosition> targets = new ArrayList<RobotMoveTargetPosition>();
		float lastAngle = currentAngle;
		for (int i = 0; i < size - 1; i++) {
			Step step1 = arr.get(i);
			Step step2 = arr.get(i + 1);

			float[] pos1 = convert2(step1.getX(), step1.getY());
			float[] pos2 = convert2(step2.getX(), step2.getY());
			float x = (pos2[0] - pos1[0]);
			float y = (pos2[1] - pos1[1]);
			float angle = (float) Math.atan2(y, x);
			float a = getAngle(angle, x, y);
			float theta = a - lastAngle;
			lastAngle = a;
			currentAngle = lastAngle;
			if (theta != 0.0f) {
				if (theta < -Math.PI) {
					theta = (float) (2 * Math.PI + theta);
				}

				if (theta > Math.PI) {
					theta = (float) (theta - 2 * Math.PI);
				}
				RobotMoveTargetPosition rot = new RobotMoveTargetPosition(0, 0,
						theta);
				targets.add(rot);
			}

			float distance = (float) Math.sqrt(x * x + y * y);
			RobotMoveTargetPosition move = new RobotMoveTargetPosition(
					distance, 0, 0);
			targets.add(move);
		}
		return targets;
	}

	public static float currentAngle;

	/**
	 * Sau khi láº¥y Ä‘Æ°á»£c cÃ¡c Ä‘iá»ƒm trung gian, tÃ­nh toÃ¡n láº¡i xem cÃ¡c Ä‘iá»ƒm nÃ o trÃªn cÃ¹ng 1 Ä‘Æ°á»�ng tháº³ng thÃ¬ láº¡i bá»� luÃ´n.
	 * @param arr
	 * @return
	 */
	public static ArrayList<Step> optimizeStep(ArrayList<Step> arr) {
		int leng = arr.size();
		if (leng <= 2) {
			return arr;
		}
		ArrayList<Step> steps = new ArrayList<Step>(leng);
		boolean inALine = false;
		int index = 0;
		int fromIndex = -1;
		while (index < leng) {
			if (inALine(arr.get(index), arr.get(index + 1), arr.get(index + 2))) {
				if (!inALine) {
					fromIndex = index;
				}
				inALine = true;
				index = index + 1;
			} else {
				if (inALine) {
					// before is aline now not aline --> increase index and
					// insert into list
					index = index + 1;
					// for (int j = fromIndex;j<=index;j++) {
					if (!steps.contains(arr.get(fromIndex))) {
						steps.add(arr.get(fromIndex));
					}
					if (!steps.contains(arr.get(index))) {
						steps.add(arr.get(index));
					}
					fromIndex = -1;
					// }
				} else {
					if (!steps.contains(arr.get(index))) {
						steps.add(arr.get(index));
					}
					index = index + 1;
					if (!steps.contains(arr.get(index))) {
						steps.add(arr.get(index));
					}
				}

				inALine = false;
			}
			if (index + 2 == leng) {
				if (inALine) {
					if (!steps.contains(arr.get(fromIndex))) {
						steps.add(arr.get(fromIndex));
					}
					if (!steps.contains(arr.get(index + 1))) {
						steps.add(arr.get(index + 1));
					}
				} else {
					if (!steps.contains(arr.get(index))) {
						steps.add(arr.get(index));
					}
					index = index + 1;
					if (!steps.contains(arr.get(index))) {
						steps.add(arr.get(index));
					}
				}
				break;
			}
		}

		return steps;
	}

	private static boolean inALine(Step step1, Step step2, Step step3) {
		if (step2.getX() == step3.getX()) {
			if (step1.getX() == step2.getX()) {
				return true;
			} else {
				return false;
			}
		}
		if (step1.getX() == step2.getX()) {
			if (step2.getX() == step3.getX()) {
				return true;
			} else {
				return false;
			}
		}

		// if (step2.getY() == step3.getY()) {
		// if (step1.getY() == step2.getY()) {
		// return true;
		// } else {
		// return false;
		// }
		// }
		// if (step1.getY() == step2.getY()) {
		// if (step2.getY() == step3.getY()) {
		// return true;
		// } else {
		// return false;
		// }
		// }

		// x differences
		double d1 = ((double) (step2.getY() - step1.getY()))
				/ ((double) (step2.getX() - step1.getX()));
		double d2 = ((double) (step3.getY() - step2.getY()))
				/ ((double) (step3.getX() - step2.getX()));
		if (d2 == d1) {
			return true;
		}
		return false;
	}

	/**
	 * TÃ­nh gÃ³c cáº§n quay Ä‘á»ƒ Ä‘áº¿n Ä‘Æ°á»£c gÃ³c angle dá»±a vÃ o toáº¡ Ä‘á»™ cá»§a vÃ©c tÆ¡ chá»‰ phÆ°Æ¡ng dx, dy 
	 * @param angle
	 * @param dx
	 * @param dy
	 * @return
	 */
	public static float getAngle(float angle, float dx, float dy) {
		float pi = (float) Math.PI;
		if (angle<-pi/2) {
			angle+=pi;
		} else if (angle>pi/2) {
			angle = pi-angle;
		}
		float a = Math.abs(angle);
		if (dx == 0 & dy == 0) {
			return 0;
		}
		if (dx > 0 && dy > 0) {
			return -(2 * pi - a);
		}

		if (dx == 0 && dy > 0) {
			return -(2 * pi - pi / 2);
		}

		if (dx < 0 && dy > 0) {
			return -(pi + a);
		}

		if (dx < 0 && dy == 0) {
			return -pi;
		}

		if (dx < 0 && dy < 0) {
			return -(pi - a);
		}

		if (dx == 0 && dy < 0) {
			return -pi / 2;
		}

		if (dx > 0 && dy < 0) {
			return -a;
		}

		if (dx > 0 && dy == 0) {
			return 0;
		}
		return 0;
	}

	private static float getAngle2(float angle, float dx, float dy) {
		float pi = (float) Math.PI;
		float a = Math.abs(angle);
		if (dx == 0 & dy == 0) {
			return 0;
		}
		if (dx > 0 && dy > 0) {
			return a * -1;
		}

		if (dx == 0 && dy > 0) {
			return -pi / 2;
		}

		if (dx < 0 && dy > 0) {
			return -(pi - a);
		}

		if (dx < 0 && dy == 0) {
			return -pi;
		}

		if (dx < 0 && dy < 0) {
			return -(pi + a);
		}

		if (dx == 0 && dy < 0) {
			return -3 * pi / 2;
		}

		if (dx > 0 && dy < 0) {
			return -2 * pi + a;
		}

		if (dx > 0 && dy == 0) {
			return 0;
		}
		return 0;
	}
	
	public static float getCurrentTheta() {
		float theta = DimenConvert.currentAngle * -1;
		System.out.println("final theta " + theta);
		if (theta != 0.0f) {
			if (theta < -Math.PI) {
				theta = (float) (2 * Math.PI + theta);
			}

			if (theta > Math.PI) {
				theta = (float) (theta - 2 * Math.PI);
			}
		
		}
		return theta;
	}
}
