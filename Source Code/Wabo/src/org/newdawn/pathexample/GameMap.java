package org.newdawn.pathexample;

import org.newdawn.slick.util.pathfinding.Mover;
import org.newdawn.slick.util.pathfinding.TileBasedMap;

;

/**
 * The data map from our example game. This holds the state and context of each
 * tile on the map. It also implements the interface required by the path
 * finder. It's implementation of the path finder related methods add specific
 * handling for the types of units and terrain in the example game.
 * 
 * @author Kevin Glass
 */
public class GameMap implements TileBasedMap {
	/** The map width in tiles */
	public static final int WIDTH_REL = 600;
	/** The map height in tiles */
	public static final int HEIGHT_REL = 600;
	
	public static final int SIZE_OF_BLOCK = 25;
	/** The map width in tiles */
	public static final int WIDTH = WIDTH_REL/SIZE_OF_BLOCK -1;
	/** The map height in tiles */
	public static final int HEIGHT = HEIGHT_REL/SIZE_OF_BLOCK -1;;

	/** Indicate grass terrain at a given location */
	public static final int GRASS = 0;
	/** Indicate block terrain at a given location */
	public static final int BLOCK = 2;
	/** Indicate a wabo is at a given location */
	public static final int WABO = 5;

	/** The terrain settings for each tile in the map */
	private int[][] terrain = new int[WIDTH][HEIGHT];
	/** The unit in each tile of the map */
	private int[][] units = new int[WIDTH][HEIGHT];
	/** Indicator if a given tile has been visited during the search */
	private boolean[][] visited = new boolean[WIDTH][HEIGHT];

	/**
	 * Create a new test map with some default configuration
	 */
	public GameMap() {
		// create some test data

		// fillArea(0,0,5,5,WATER);
		// fillArea(0,5,3,10,WATER);
		// fillArea(0,5,3,10,WATER);
		// fillArea(0,15,7,15,WATER);
		// fillArea(7,26,22,4,WATER);
		//
		// fillArea(17,5,10,3,TREES);
		// fillArea(20,8,5,3,TREES);
		//
		// fillArea(8,2,7,3,TREES);
		// fillArea(10,5,3,3,TREES);

		// group 1
		fillArea(4, 1, 3, 3, BLOCK);

		fillArea(4, 4, 3, 3, BLOCK);
		
//		fillArea(3, 7, 1, 1, BLOCK);
//		fillArea(3, 6, 1, 1, BLOCK);
//		fillArea(2, 7, 1, 1, BLOCK);
//		fillArea(3, 8, 1, 1, BLOCK);
//		fillArea(4, 7, 1, 1, BLOCK);
		fillArea(2, 6, 3, 3, BLOCK);
		
		fillArea(6, 6, 3, 3, BLOCK);
//		fillArea(7, 7, 1, 1, BLOCK);
//		fillArea(7, 6, 1, 1, BLOCK);
//		fillArea(6, 7, 1, 1, BLOCK);
//		fillArea(8, 7, 1, 1, BLOCK);
//		fillArea(7, 8, 1, 1, BLOCK);

		// group 2
		fillArea(16, 1, 3, 3, BLOCK);

		fillArea(16, 4, 3, 3, BLOCK);
		fillArea(14, 6, 3, 3, BLOCK);
//		fillArea(15, 7, 1, 1, BLOCK);
//		fillArea(14, 7, 1, 1, BLOCK);
//		fillArea(15, 6, 1, 1, BLOCK);
//		fillArea(16, 7, 1, 1, BLOCK);
//		fillArea(15, 8, 1, 1, BLOCK);
		
		fillArea(18, 6, 3, 3, BLOCK);
//		fillArea(19, 7, 1, 1, BLOCK);
//		fillArea(18, 7, 1, 1, BLOCK);
//		fillArea(19, 6, 1, 1, BLOCK);
//		fillArea(20, 7, 1, 1, BLOCK);
//		fillArea(19, 8, 1, 1, BLOCK);

		// group 3
		fillArea(10, 8, 3, 2, BLOCK);

		fillArea(10, 10, 3, 3, BLOCK);
		fillArea(8, 12, 3, 3, BLOCK);
//		fillArea(9, 13, 1, 1, BLOCK);
//		fillArea(9, 12, 1, 1, BLOCK);
//		fillArea(8, 13, 1, 1, BLOCK);
//		fillArea(9, 14, 1, 1, BLOCK);
//		fillArea(10, 13, 1, 1, BLOCK);
		
		fillArea(12, 12, 3, 3, BLOCK);
//		fillArea(13, 13, 1, 1, BLOCK);
//		fillArea(12, 13, 1, 1, BLOCK);
//		fillArea(13, 12, 1, 1, BLOCK);
//		fillArea(14, 13, 1, 1, BLOCK);
//		fillArea(13, 14, 1, 1, BLOCK);
		

		// group 4
		fillArea(4, 13, 3, 3, BLOCK);

		fillArea(4, 16, 3, 3, BLOCK);
		fillArea(2, 18, 3, 3, BLOCK);
//		fillArea(3, 19, 1, 1, BLOCK);
//		fillArea(3, 18, 1, 1, BLOCK);
//		fillArea(2, 19, 1, 1, BLOCK);
//		fillArea(4, 19, 1, 1, BLOCK);
//		fillArea(3, 20, 1, 1, BLOCK);
		
		fillArea(6, 18, 3, 3, BLOCK);
//		fillArea(7, 19, 1, 1, BLOCK);
//		fillArea(7, 18, 1, 1, BLOCK);
//		fillArea(6, 19, 1, 1, BLOCK);
//		fillArea(7, 20, 1, 1, BLOCK);
//		fillArea(8, 19, 1, 1, BLOCK);

		// group 5
		fillArea(16, 13, 3, 3, BLOCK);

		fillArea(16, 16, 3, 3, BLOCK);
		fillArea(14, 18, 3, 3, BLOCK);
//		fillArea(15, 19, 1, 1, BLOCK);
//		fillArea(14, 19, 1, 1, BLOCK);
//		fillArea(15, 18, 1, 1, BLOCK);
//		fillArea(16, 19, 1, 1, BLOCK);
//		fillArea(15, 20, 1, 1, BLOCK);
		
		fillArea(18, 18, 3, 3, BLOCK);
//		fillArea(19, 19, 1, 1, BLOCK);
//		fillArea(18, 19, 1, 1, BLOCK);
//		fillArea(19, 18, 1, 1, BLOCK);
//		fillArea(20, 19, 1, 1, BLOCK);
//		fillArea(19, 20, 1, 1, BLOCK);
		
		//units[1][1] = WABO;
		//units[21][1] = WABO;
		// units[2][7] = BOAT;
		// units[20][25] = PLANE;
	}

	/**
	 * Fill an area with a given terrain type
	 * 
	 * @param x
	 *            The x coordinate to start filling at
	 * @param y
	 *            The y coordinate to start filling at
	 * @param width
	 *            The width of the area to fill
	 * @param height
	 *            The height of the area to fill
	 * @param type
	 *            The terrain type to fill with
	 */
	private void fillArea(int x, int y, int width, int height, int type) {
		for (int xp = x; xp < x + width; xp++) {
			for (int yp = y; yp < y + height; yp++) {
				terrain[xp][yp] = type;
			}
		}
	}

	/**
	 * Clear the array marking which tiles have been visted by the path finder.
	 */
	public void clearVisited() {
		for (int x = 0; x < getWidthInTiles(); x++) {
			for (int y = 0; y < getHeightInTiles(); y++) {
				visited[x][y] = false;
			}
		}
	}

	/**
	 * @see TileBasedMap#visited(int, int)
	 */
	public boolean visited(int x, int y) {
		return visited[x][y];
	}

	/**
	 * Get the terrain at a given location
	 * 
	 * @param x
	 *            The x coordinate of the terrain tile to retrieve
	 * @param y
	 *            The y coordinate of the terrain tile to retrieve
	 * @return The terrain tile at the given location
	 */
	public int getTerrain(int x, int y) {
		return terrain[x][y];
	}

	/**
	 * Get the unit at a given location
	 * 
	 * @param x
	 *            The x coordinate of the tile to check for a unit
	 * @param y
	 *            The y coordinate of the tile to check for a unit
	 * @return The ID of the unit at the given location or 0 if there is no unit
	 */
	public int getUnit(int x, int y) {
		return units[x][y];
	}

	/**
	 * Set the unit at the given location
	 * 
	 * @param x
	 *            The x coordinate of the location where the unit should be set
	 * @param y
	 *            The y coordinate of the location where the unit should be set
	 * @param unit
	 *            The ID of the unit to be placed on the map, or 0 to clear the
	 *            unit at the given location
	 */
	public void setUnit(int x, int y, int unit) {
		units[x][y] = unit;
	}

	/**
	 * @see TileBasedMap#blocked(Mover, int, int)
	 */
	public boolean blocked(Mover mover, int x, int y) {
		// if theres a unit at the location, then it's blocked

		if (getUnit(x, y) != 0) {
			return true;
		}

		int unit = ((UnitMover) mover).getType();

		// tanks can only move across grass

		if (unit == WABO) {
			return terrain[x][y] != GRASS;
		}

		// unknown unit so everything blocks

		return true;
	}

	/**
	 * @see TileBasedMap#getCost(Mover, int, int, int, int)
	 */
	public float getCost(Mover mover, int sx, int sy, int tx, int ty) {
		if (sx == tx || sy == ty) {
			return 1;
		} else {
			return (float) Math.sqrt(2);
		}
	}

	/**
	 * @see TileBasedMap#getHeightInTiles()
	 */
	public int getHeightInTiles() {
		return WIDTH;
	}

	/**
	 * @see TileBasedMap#getWidthInTiles()
	 */
	public int getWidthInTiles() {
		return HEIGHT;
	}

	/**
	 * @see TileBasedMap#pathFinderVisited(int, int)
	 */
	public void pathFinderVisited(int x, int y) {
		visited[x][y] = true;
	}

}
