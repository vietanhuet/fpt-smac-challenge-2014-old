package uet.invincible.algorithm;

public class PointD {
	public float x;
	public float y;
	public float theta;
	public PointD(float x, float y, float theta) {
		this.x = x;
		this.y = y;
		this.theta = theta;
	}
	public PointD(float x, float y) {
		this.x = x;
		this.y = y;
		theta = 0;
	}
	public float getTheta() {
		return theta;
	}
	public void setTheta(float theta) {
		this.theta = theta;
	}
	public PointD() {
		x = 0;
		y = 0;
		theta = 0;
	}
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	
}
