package uet.invincible.algorithm;

import java.util.ArrayList;

import com.fpt.robot.types.RobotPose2D;

/**
 * Created by Hoang on 23/09/2014.
 */

public class Wabo {
	
	
    // declare important point
    public static final RobotPose2D startPoint1 = new RobotPose2D(0, -2.5f, 0, true);
    public static final RobotPose2D startPoint2 = new RobotPose2D(0, 2.5f, 0, true);
    public static final RobotPose2D topCenter = new RobotPose2D(-2, 0, 0, true);
    public static final RobotPose2D rightPoint = new RobotPose2D(0, 1.5f, 0, true);
    public static final RobotPose2D leftPoint = new RobotPose2D(2, 0, 0, true);
    public static final RobotPose2D bottomCenter = new RobotPose2D(0, -1.5f, 0, true);
    public static final RobotPose2D bottomLeft = new RobotPose2D(2.5f, -2.5f, 0, true);
    public static final RobotPose2D bottomRight = new RobotPose2D(2.5f, 2.5f, 0, true);

    // Declare safe zone
    public static final ArrayList<RobotPose2D> safeZone = new ArrayList<RobotPose2D>() {{
        add(startPoint1);
        add(startPoint2);
        add(topCenter);
        add(leftPoint);
        add(rightPoint);
        add(bottomCenter);
    }};

    // declare target point
    public static final RobotPose2D table1 = new RobotPose2D(0, 0, 0, true);
    public static final RobotPose2D table2 = new RobotPose2D(1.5f, 1.5f, 0, true);
    public static final RobotPose2D table3 = new RobotPose2D(-1.5f, 1.5f, 0, true);
    public static final RobotPose2D table4 = new RobotPose2D(-1.5f, -1.5f, 0, true);
    public static final RobotPose2D table5 = new RobotPose2D(1.5f, -1.5f, 0, true);
    public static final RobotPose2D dishesTableLeft = new RobotPose2D(2.5f, -2, 0, true);
    public static final RobotPose2D dishesTableCenter = new RobotPose2D(2.5f, 0, 0, true);
    public static final RobotPose2D dishesTableRight = new RobotPose2D(2.5f, 2, 0, true);

    //add target point to list :(
    public static final ArrayList<RobotPose2D> target = new ArrayList<RobotPose2D>(){{
        add(table1);
        add(table2);
        add(table3);
        add(table4);
        add(table5);
        add(dishesTableLeft);
        add(dishesTableCenter);
        add(dishesTableRight);
    }};

    //set radius for safe zone
    public static final double zoneRadius = 0.5;


    // distance from point a to point b
    public static double distance(RobotPose2D a, RobotPose2D b) {
        double result;
        result = Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
        return result;
    }

    // check if point b inside safe zone a
    public static boolean inside(RobotPose2D a, RobotPose2D b) {
        if( a.x + zoneRadius >= b.x && a.y + zoneRadius >= b.y ) {
            return  true;
        }
        return  false;
    }

    //check if target accessible from a zone "A" to target B
    public static boolean accessible(RobotPose2D a, RobotPose2D b) {
        if( a.equals(safeZone.get(0)) ) {
            if( b.equals(target.get(0)) ) {
                return true;
            } return false;
        }
        if( a.equals(safeZone.get(1)) ) {
            if( b.equals(target.get(1)) ) {
                return true;
            } return false;
        }
        if( a.equals(safeZone.get(2)) ) {
            if( b.equals(target.get(0)) || b.equals(target.get(1)) ) {
                return true;
            } return false;
        }
        if( a.equals(safeZone.get(3)) ) {
            if( b.equals(target.get(1)) || b.equals(target.get(2)) ) {
                return true;
            } return false;
        }
        if( a.equals(safeZone.get(4)) ) {
            if( b.equals(target.get(2)) || b.equals(target.get(3)) || b.equals(target.get(4)) ) {
                return true;
            } return false;
        }
        if( a.equals(safeZone.get(5)) ) {
            if( b.equals(target.get(0)) || b.equals(target.get(2)) ) {
                return true;
            } return false;
        }
        return false;
    }

    public ArrayList<RobotPose2D> getPath(RobotPose2D from, RobotPose2D to) {
        ArrayList<RobotPose2D> path = new ArrayList<RobotPose2D>();

        // special fixed path
        if (inside(from, safeZone.get(0))) {
            if ( to.equals(target.get(5)) || to.equals(target.get(6)) || to.equals(target.get(7)) ) {
                path.add(bottomLeft);
                path.add(to);
                return path;
            }
        }

        if (inside(from, safeZone.get(1))) {
            if ( to.equals(target.get(5)) || to.equals(target.get(6)) || to.equals(target.get(7)) ) {
                path.add(bottomRight);
                path.add(to);
                return path;
            }
        }

        // check if it start from one of the safe zone
        int startZone = -1;
        for( int i=0; i<safeZone.size(); i++) {
            if( inside(safeZone.get(i), from) ) {
                startZone = i;
                break;
            }
        }

        // if it a random point on map
        if( startZone == -1 ) {
            double minDistance = 1000000;
            for(int i = 2; i<=safeZone.size(); i++) {
                double curDistance = distance(safeZone.get(i), from);
                // assume the nearest is accessible zone
                if( curDistance < minDistance ) {
                    minDistance = curDistance;
                    startZone = i;
                }
            }
            // move to start zone
            path.add( safeZone.get(startZone) );
        }

        // if start from a zone
        if( !accessible(safeZone.get(startZone), to)) {
            // find the nearest safe zone that accessible to target
            double minDistance = 1000000;
            RobotPose2D nextSafeZone = safeZone.get(startZone);
            for(int i=2; i<=safeZone.size(); i++) {
                if( i!= startZone ) {
                    RobotPose2D cur = safeZone.get(i);
                    double curDistance =  distance(safeZone.get(startZone), cur);
                    if( accessible(cur, to) && curDistance < minDistance) {
                        minDistance = curDistance;
                        nextSafeZone = cur;
                    }
                }
            }
            path.add(nextSafeZone);
        }

        path.add(to);
        return path;
    }
    
    public static void main(String[] args) {
        Wabo newWabo = new Wabo();
        // start point checking
        RobotPose2D start = new RobotPose2D(-2.5f, -2.5f, 0, true);

        // destination point checking
        RobotPose2D to = new RobotPose2D(2.5f, 2.5f, 0, true);
//
//        //check distance
//        double testVar = newWabo.distance(start, to);
//        System.out.printf("%.1f \n", testVar);

        ArrayList<RobotPose2D> path = newWabo.getPath(start, to);
        for (int i=0; i<path.size(); i++) {
            System.out.printf("%f %f \n", path.get(i).x, path.get(i).y);
        }
    }
}
