package uet.invincible.wabo;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fpt.robot.Robot;
import com.fpt.robot.RobotException;
import com.fpt.robot.RobotInfo;
import com.fpt.robot.app.RobotActivity;
import com.fpt.robot.motion.RobotMotionLocomotionController;
import com.fpt.robot.types.RobotMoveTargetPosition;
import com.fpt.robot.types.RobotPose2D;
import com.fpt.robot.vision.RobotObjectDetection;
import com.fpt.robot.vision.RobotObjectDetection.DetectedObject;
import com.fpt.robot.vision.RobotObjectDetection.ObjectPose;
import com.fpt.robot.wabo.WaboArm;
import com.fpt.robot.wabo.WaboSMAC;

public class TestActivity extends RobotActivity implements
		RobotObjectDetection.Listener, OnClickListener {

	// object detection variables
	private RobotObjectDetection.Monitor mDetectMonitor;
	private boolean mDetectedObject = false;
	private Timer timerDetect;

	private Robot robot;
	private EditText mEdX, mEdY, mEdTheta;
	private boolean wakeUp = false;

	public Button start;
	public EditText x;
	public EditText y;
	public EditText theta;
	
	public RobotPose2D convertCoordirates(RobotPose2D currentPos, RobotPose2D destinationPos) {
		float x = (float) ((destinationPos.x-currentPos.x) * Math.cos(currentPos.theta) + (destinationPos.y-currentPos.y) * Math.sin(currentPos.theta));
		float y = (float) (-(destinationPos.x-currentPos.x) * Math.sin(currentPos.theta) + (destinationPos.y-currentPos.y) * Math.cos(currentPos.theta));
		float theta = destinationPos.theta - currentPos.theta;
		return new RobotPose2D(x, y, theta, true);
	}
	public void calculatorPath(RobotPose2D currentPos) {
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		wakeUp = false;
		timerDetect = new Timer();
		setContentView(R.layout.activity_test);
		mEdX = (EditText) findViewById(R.id.test_x);
		mEdY = (EditText) findViewById(R.id.test_y);
		mEdTheta = (EditText) findViewById(R.id.test_theta);
		start = (Button) findViewById(R.id.start);
		start.setOnClickListener(this);
		x = (EditText) findViewById(R.id.x);
		y = (EditText) findViewById(R.id.y);
		theta = (EditText) findViewById(R.id.theta);
		
		
//		try {
//			RobotPose2D crr = new RobotPose2D();
//			crr = WaboSMAC.relocation(getRobot());
//			currentPos.x = crr.x;
//			currentPos.y = crr.y;
//			currentPos.theta = crr.theta;
//			x.setText(currentPos.x + " " + currentPos.y + " " + currentPos.theta);
//		} catch (RobotException e) {
//			e.printStackTrace();
//		}
	}

	//get value from edit text
	private float getX() {
		String x = mEdX.getText().toString();
		if (TextUtils.isEmpty(x)) {
			return 0;
		}
		return Float.parseFloat(x);
	}

	private float getY() {
		String y = mEdY.getText().toString();
		if (TextUtils.isEmpty(y)) {
			return 0;
		}
		return Float.parseFloat(y);
	}

	private float getTheta() {
		String theta = mEdTheta.getText().toString();
		if (TextUtils.isEmpty(theta)) {
			return 0;
		}
		return Float.parseFloat(theta);
	}

	//callback
	@Override
	public void onRobotServiceConnected() {
		super.onRobotServiceConnected();
		robot = getConnectedRobot();
		new Thread(new Runnable() {
			@Override
			public void run() {
				if (robot != null) {
					try {
						makeToast("On Service connected Register monitor");
						mDetectMonitor = new RobotObjectDetection.Monitor(
								robot, TestActivity.this);
						mDetectMonitor.start();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

	@Override
	public void onNetworkConnected(boolean connected) {
		super.onNetworkConnected(connected);
		if (!connected) {
			robot = null;
			wakeUp = false;
		}
	}

	@Override
	public void onRobotConnected(String addr, int port) {
		super.onRobotConnected(addr, port);
		try {
			if (robot != null) {
				if (mDetectMonitor != null) {
					RobotObjectDetection.stopDetection(getRobot());
					mDetectMonitor.stop();
				}
			}
		} catch (RobotException e) {
		}
		try {
			makeToast("On robot connected Register monitor");
			robot = getConnectedRobot();
			mDetectMonitor = new RobotObjectDetection.Monitor(robot, this);
			boolean b = mDetectMonitor.start();
			System.out.println("On robot connected Register monitor " + b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		robot = getConnectedRobot();

	}

	private void makeToast(final String text) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(TestActivity.this, text, Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public void onRobotDisconnected(String addr, int port) {
		super.onRobotDisconnected(addr, port);
		if (robot != null) {
			RobotInfo info = robot.getInfo();
			if (info != null) {
				if (info.getIpAddress().equalsIgnoreCase(addr)) {
					robot = null;
				}
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.motion, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_scan) {
			scanRobot();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {

		try {
			if (mDetectMonitor != null) {
				mDetectMonitor.stop();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			if (robot != null) {
				RobotObjectDetection.stopDetection(getRobot());
			}
		} catch (RobotException e) {
			e.printStackTrace();
		}
		super.onDestroy();
		wakeUp = false;
	}

	//click events
	public void onMove(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}

		final float x = getX();
		final float y = getY();
		final float theta = getTheta();

		System.out.println("x " + x + " y " + y + " theta " + theta);
		new Thread(new Runnable() {
			@Override
			public void run() {

				WaboUtils.move(x, y, theta, wakeUp, robot);
				wakeUp = true;
			}
		}).start();
	}

	public void onObjectDetect() {
		if (robot == null) {
			scanRobot();
			return;
		}
		mDetectedObject = false;

		makeToast("Start detection");
		try {
			RobotObjectDetection.startDetection(getRobot());
			timerDetect = new Timer();
			timerDetect.schedule(new TimerTask() {
				@Override
				public void run() {
					doDetectWhenTimeout();
				}
			}, 2000);
		} catch (RobotException e) {
			e.printStackTrace();
		}

	}

	/**
	 * timeout: rotate robot to check object
	 */
	private void doDetectWhenTimeout() {
		if (!mDetectedObject) {
			System.out.println("Timeout detect");
			try {
				RobotMotionLocomotionController
						.moveWithVelocity(getRobot(), 0, 0.2f);
			} catch (RobotException e) {
				makeToast("move when detect timeout fail: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	
	public void onArmInit(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}

		new Thread(new Runnable() {
			
			@Override
			public void run() {
				//return arm to init
				WaboUtils.armInit(getRobot());
				
			}
		}).start();
	}

	public void onGetObject(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				WaboUtils.getObject(getRobot());
			}
		}).start();
		

	}

	public void onMoveCloser(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}
		if (mDetectedObject) {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					WaboUtils.moveCloserObject(mPose, mID, robot);
					
				}
			}).start();
		}
	}

	public void onPutObject(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}
		new Thread(new Runnable() {

			@Override
			public void run() {
				WaboUtils.putObject(getRobot());
			}
		}).start();

	}

	ObjectPose mPose;
	int mID = 2;

	/**
	 * detect marker succeed
	 */
	@Override
	public void onObjectsDetected(final ArrayList<DetectedObject> objects) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				for (DetectedObject obj : objects) {
					ObjectPose pose = obj.getPose();
					int id = obj.getObjectId();
					System.out.println("ID: " + id);
					if (id == mID && !mDetectedObject) {
						mPose = pose;
						mDetectedObject = true;
						timerDetect.cancel();
						System.out.println("ID: " + id + "...." + pose.x + ", "
								+ pose.y + ", " + pose.theta);
						if (robot != null) {
							try {
								System.out.println("Stop move and detection");
								RobotObjectDetection.stopDetection(getRobot());
								RobotMotionLocomotionController.moveStop(getRobot());
								// goToObject(pose.x, pose.y, pose.theta, id);
								break;
							} catch (RobotException e) {
								e.printStackTrace();
							}
						}
					}
				}
				WaboUtils.moveCloserObject(mPose, 1, getRobot());
				WaboUtils.getObject(getRobot());
				WaboUtils.move(-1, 0, 0, wakeUp, getRobot());
				WaboUtils.putObject(getRobot());
			}
		}).start();
	}

	private void goToObject(float x, float y, float angle, int id) {
		double distance = Math.sqrt(x * x + y * y);
		try {
			if (distance > 0.6f) {
				System.out.println("move to x-0.6");
				RobotMotionLocomotionController.moveTo(robot,
						new RobotMoveTargetPosition(x - 0.7f, y, 0.0f));
			}
			System.out.println("move closer to object, id: " + id);
			boolean result = WaboSMAC.moveCloserToObject(robot, id);
			// navigateToOrigin();
			// result = false;
			if (result) {
				System.out.println("approach object");
				result = WaboArm.runGesture(getRobot(), "APPROACH_OBJ");
				result = WaboSMAC.attachObject(getRobot());
				result = WaboArm.runGesture(getRobot(), "PICK_UP");

			} else {
				makeToast("Move closer fail");
			}
		} catch (RobotException e) {
			e.printStackTrace();
		}
	}
	
	public RobotPose2D currentPos = new RobotPose2D(0, (float)-2.5, 0, true);
	public boolean relocate = false;
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.start:
//			if(!relocate) {
//				relocate = true;
//				try {
//					RobotPose2D crr = new RobotPose2D();
//					crr = WaboSMAC.relocation(getRobot());
//					currentPos.x = crr.x;
//					currentPos.y = crr.y;
//					currentPos.theta = crr.theta;
//					x.setText(currentPos.x + " " + currentPos.y + " " + currentPos.theta);
//				} catch (RobotException e) {
//					e.printStackTrace();
//				}
//			}
			
			new Thread(new Runnable() {
				@Override
				public void run() {
//					try {
						currentPos = new RobotPose2D(0, (float)-2.5, 0, true);
						RobotPose2D m1 = convertCoordirates(currentPos, new RobotPose2D(1, 0, 0, true));
//						makeToast("Current: " + currentPos.x + " " + currentPos.y + " " + currentPos.theta);
						
						makeToast("xxxxx: " + m1.x + " " + m1.y + " " + m1.theta);
						
//						WaboUtils.move(m1.x, m1.y, m1.theta, wakeUp, getRobot());
						
//						currentPos = WaboSMAC.getCurrentPose(getRobot());
						currentPos = new RobotPose2D(1, 0, 0, true);
						makeToast("Current: " + currentPos.x + " " + currentPos.y + " " + currentPos.theta);
						
						m1 = convertCoordirates(currentPos, new RobotPose2D((float)2.5, 0, 0, true));
//						WaboUtils.move(m1.x, m1.y, m1.theta, wakeUp, getRobot());
						
//						currentPos = WaboSMAC.getCurrentPose(getRobot());
						makeToast("Current: " + currentPos.x + " " + currentPos.y + " " + currentPos.theta);

						
						onObjectDetect();
						
						wakeUp = true;
//					} catch (RobotException e) {
//						e.printStackTrace();
//					}
				}
			}).start();
			break;
		default:
			break;
		}
		
	}

}
