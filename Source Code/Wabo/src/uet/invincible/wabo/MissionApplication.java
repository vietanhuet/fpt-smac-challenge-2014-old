package uet.invincible.wabo;

import java.util.ArrayList;

import org.newdawn.pathexample.GameMap;
import org.newdawn.pathexample.UnitMover;
import org.newdawn.slick.util.pathfinding.AStarPathFinder;
import org.newdawn.slick.util.pathfinding.DimenConvert;
import org.newdawn.slick.util.pathfinding.Mover;
import org.newdawn.slick.util.pathfinding.Path;
import org.newdawn.slick.util.pathfinding.Path.Step;
import org.newdawn.slick.util.pathfinding.PathFinder;

import com.fpt.robot.app.RobotApplication;
import com.fpt.robot.types.RobotMoveTargetPosition;

public class MissionApplication extends RobotApplication {
	public float currentWaboAngle = 0;
	public Step lastStep;
	public Step currentStep;
	private GameMap map = new GameMap();
	/** The path finder we'll use to search our map */
	private PathFinder finder;
	/** The last path found for the current unit */
	private Path path;

	@Override
	public void onCreate() {
		super.onCreate();
		finder = new AStarPathFinder(map, 500, true);
//		map.setUnit(1, 1, GameMap.WABO);
//		findPath(new UnitMover(GameMap.WABO), 1, 1, 1, 18);
//		resetState(1, 1, 1, 18);
//		System.out.println("last " + map.getUnit(1, 1));
//		System.out.println("current " + map.getUnit(1, 18));
//		findPath(new UnitMover(GameMap.WABO), 1, 18, 22, 22);
//		if (path != null) {
//			ArrayList<Step> arr = DimenConvert.optimizeStep(path.getSteps());
//			for (Step s : arr) {
//				System.out.println("step " + s.getX() + " " + s.getY());
//			}
//			ArrayList<RobotMoveTargetPosition> targets = DimenConvert.calculateTargetPosition(arr);
//			for (RobotMoveTargetPosition s : targets) {
//				System.out.println("target " + s.x + " " + s.y);
//			}
//		}
	}

	public Path getPath() {
		return path;
	}

	public void findPath(Mover move, int fromX, int fromY, int toX, int toY) {
		map.clearVisited();
		path = finder.findPath(move, fromX, fromY, toX, toY);
		if (path != null) {
			lastStep = path.getStep(0);
			currentStep = path.getStep(path.getLength() - 1);
		}
	}
	
	public Path findPath( int fromX, int fromY, int toX, int toY) {
		map.clearVisited();
		path = finder.findPath(new UnitMover(GameMap.WABO), fromX, fromY, toX, toY);
		if (path != null) {
			lastStep = path.getStep(0);
			currentStep = path.getStep(path.getLength() - 1);
		}
		
		return path;
	}
	
	public void setPositionWabo(int x, int y) {
		map.setUnit(x, y, GameMap.WABO);
	}
	
	public void setPositionGrass(int x, int y) {
		map.setUnit(x, y, GameMap.GRASS);
	}

	public void resetState(int fromX, int fromY, int toX, int toY) {
		int unit = map.getUnit(fromX, fromY);
		map.setUnit(fromX, fromX, 0);
		map.setUnit(toX, toY, unit);
	}
}
