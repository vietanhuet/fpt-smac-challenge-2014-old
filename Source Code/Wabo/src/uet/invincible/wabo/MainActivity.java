package uet.invincible.wabo;

import uet.invincible.wabo.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.fpt.robot.app.RobotActivity;

public class MainActivity extends RobotActivity {

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void onTest(View v) {
		Intent i = new Intent(this, TestActivity.class);
		startActivity(i);
		finish();
	}
	public void onMission(View v) {
		Intent i = new Intent(this, MissionActivity.class);
		startActivity(i);
		finish();
	}
}
