package uet.invincible.wabo;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.newdawn.slick.util.pathfinding.DimenConvert;
import org.newdawn.slick.util.pathfinding.Path;
import org.newdawn.slick.util.pathfinding.Path.Step;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fpt.robot.Robot;
import com.fpt.robot.RobotException;
import com.fpt.robot.RobotInfo;
import com.fpt.robot.app.RobotActivity;
import com.fpt.robot.motion.RobotMotionLocomotionController;
import com.fpt.robot.motion.RobotMotionStiffnessController;
import com.fpt.robot.types.RobotMoveTargetPosition;
import com.fpt.robot.types.RobotPose2D;
import com.fpt.robot.vision.RobotObjectDetection;
import com.fpt.robot.vision.RobotObjectDetection.DetectedObject;
import com.fpt.robot.vision.RobotObjectDetection.ObjectPose;
import com.fpt.robot.wabo.WaboSMAC;
import com.fpt.robot.wabo.WaboSensors;
import com.fpt.robot.wabo.WaboSensors.BumperPosition;
import com.fpt.robot.wabo.WaboSensors.ButtonPosition;
import com.fpt.robot.wabo.WaboSensors.CliffPosition;
import com.fpt.robot.wabo.WaboSensors.ProximityPosition;
import com.fpt.robot.wabo.WaboSensors.ProximityState;
import com.fpt.robot.wabo.WaboSensors.SonarPosition;
import com.fpt.robot.wabo.WaboSensors.WheelPosition;

/**
 * Demo wabo lÃ m nhiá»‡m vá»¥:<br> 
 * SÃ¢n thi Ä‘áº¥u mÃ£ hoÃ¡ láº¡i thÃ nh toáº¡ Ä‘á»™ lÃ  giao Ä‘iá»ƒm cá»§a cÃ¡c Ä‘Æ°á»�ng 25 cm.<br>
 * Gá»‘c toáº¡ Ä‘á»™ trÃªn cÃ¹ng bÃªn trÃ¡i, trá»¥c X hÆ°á»›ng sang trÃ¡i (ngÆ°á»£c vá»›i trá»¥c Y cáº£u toáº¡ Ä‘á»™ sÃ¢n BTC cung cáº¥p), trá»¥c Y hÆ°á»›ng theo trá»¥c X cá»§a sÃ¢n BTC cung cáº¥p.<br> 
 * NhÆ° váº­y vÃ¹ng di chuyá»ƒn chá»‰ cÃ³ thá»ƒ lÃ  6mx6m nÃªn toáº¡ Ä‘á»™ sáº½ cÃ³ thá»ƒ tá»« 0->23.<br>  
 * Vá»‹ trÃ­ xuáº¥t phÃ¡t cÃ³ thá»ƒ thay Ä‘á»•i. Vá»‹ trÃ­ Ä‘Ã­ch Ä‘áº¿n hiá»‡n táº¡i Ä‘á»ƒ lÃ  11,20.<br>
 * Sau khi láº¥y Ä‘Æ°á»£c object robot sáº½ lÃ¹i láº¡i 1m--> tiáº¿p Ä‘Ã³ lÃ  Ä‘á»‹nh vá»‹ láº¡i vá»‹ trÃ­ cá»§a mÃ¬nh.<br>
 * Sau Ä‘Ã³, di chuyá»ƒn Ä‘áº¿n 1 Ä‘iá»ƒm Ä‘Ã£ Ä‘Æ°á»£c mÃ£ hoÃ¡ trÃªn sÃ¢n gáº§n vá»‹ trÃ­ hiá»‡n táº¡i nháº¥t.<br>
 * Khi Ä‘Ã£ xÃ¡c Ä‘á»‹nh Ä‘Æ°á»£c vá»‹ trÃ­ thÃ¬ robot sáº½ di chuyá»ƒn Ä‘áº¿n vá»‹ trÃ­ bÃ n giá»¯a(toáº¡ Ä‘á»™ 11,14), tiáº¿p Ä‘Ã³ láº¥y current pose Ä‘á»ƒ tÃ­nh sai sá»‘ di chuyá»ƒn vÃ  di chuyá»ƒn Ä‘áº¿n Ä‘Ãºng toáº¡ Ä‘á»� cáº§n Ä‘áº¿n vÃ  gÃ³c hÆ°á»›ng tháº±ng Ä‘áº¿n bÃ n. 
 * Thoáº£ mÃ£n cÃ¡c Ä‘iá»�u kiá»‡n trÃªn robot sáº½ di chuyá»ƒn Ä‘áº¿n bÃ n cho Ä‘áº¿n khi bumper cháº¡m bÃ n (dÃ¹ng hÃ m <b>moveWithVelocity</b>)  
 *   
 * @author trungnd
 *
 */
public class MissionActivity extends RobotActivity implements
		RobotObjectDetection.Listener, WaboSensors.Listener {

	// object detection variables
	private RobotObjectDetection.Monitor mDetectMonitor;
	private WaboSensors.Monitor mSensorsMonitor;
	private boolean mDetectedObject = false;
	private Timer timerDetect;
	private TextView mTvLog;
	private Robot robot;
	private Spinner mSpinnerX, mSpinnerY, mSpinnerToX, mSpinnerToY,
			mSpinnerTable, mSpinnerObject;
	private boolean wakeUp = false;
	private int fromX = 21;
	private int fromY = 7;
	private int toX = 11;
	private int toY = 20;
	private int table = 2;
	private int object = 2;
	private RobotMoveTargetPosition currentPosition;
	private RobotPose2D currentPose;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		wakeUp = false;
		timerDetect = new Timer();
		setContentView(R.layout.activity_mission);
		mSpinnerX = (Spinner) findViewById(R.id.spin_from_x);
		mSpinnerY = (Spinner) findViewById(R.id.spin_from_y);

		mSpinnerToX = (Spinner) findViewById(R.id.spin_to_x);
		mSpinnerToY = (Spinner) findViewById(R.id.spin_to_y);
		mSpinnerTable = (Spinner) findViewById(R.id.spin_table);
		mSpinnerObject = (Spinner) findViewById(R.id.spin_object);
		mTvLog = (TextView) findViewById(R.id.tv_log);
		mSpinnerX.setSelection(fromX);
		mSpinnerY.setSelection(fromY);

		mSpinnerToX.setSelection(toX);
		mSpinnerToY.setSelection(toY);

		mSpinnerTable.setSelection(table - 1);
		mSpinnerObject.setSelection(object);
	}

	private int getSelection(Spinner sp) {
		if (sp != null) {
			int i = Integer.parseInt(sp.getSelectedItem().toString());
			return i;
		}
		return 0;
	}

	// callback
	@Override
	public void onRobotServiceConnected() {
		super.onRobotServiceConnected();
		robot = getConnectedRobot();
		new Thread(new Runnable() {
			@Override
			public void run() {
				if (robot != null) {
					try {
						makeToast("On Service connected Register monitor");
						mDetectMonitor = new RobotObjectDetection.Monitor(
								robot, MissionActivity.this);
						mDetectMonitor.start();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		}).start();
	}

	@Override
	public void onNetworkConnected(boolean connected) {
		super.onNetworkConnected(connected);
		if (!connected) {
			robot = null;
			wakeUp = false;
		}

		if (mSensorsMonitor != null) {
			// TODO RobotObjectDetection.stopDetection(robot);
			try {
				stopSensorsMonitor();
			} catch (RobotException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onRobotConnected(String addr, int port) {
		super.onRobotConnected(addr, port);
		robot = getConnectedRobot();
		try {
			if (robot != null) {
				if (mDetectMonitor != null) {
					RobotObjectDetection.stopDetection(robot);
					mDetectMonitor.stop();
				}

				if (mSensorsMonitor != null) {
					// TODO RobotObjectDetection.stopDetection(robot);
					stopSensorsMonitor();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			makeToast("On robot connected Register monitor");
			mDetectMonitor = new RobotObjectDetection.Monitor(robot, this);

			boolean b = mDetectMonitor.start();
			System.out.println("On robot connected Register monitor " + b);
			log("On robot connected Register monitor " + b);

			mSensorsMonitor = new WaboSensors.Monitor(robot, this);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void makeToast(final String text) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(MissionActivity.this, text, Toast.LENGTH_SHORT)
						.show();
			}
		});
	}

	@Override
	public void onRobotDisconnected(String addr, int port) {
		super.onRobotDisconnected(addr, port);
		if (robot != null) {
			RobotInfo info = robot.getInfo();
			if (info != null) {
				if (info.getIpAddress().equalsIgnoreCase(addr)) {
					robot = null;
				}
			}

			if (mSensorsMonitor != null) {
				try {
					stopSensorsMonitor();
				} catch (RobotException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.motion, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_scan) {
			scanRobot();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {

		try {
			if (mDetectMonitor != null) {
				mDetectMonitor.stop();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			if (mSensorsMonitor != null) {
				mSensorsMonitor.stop();
				mSensorsMonitor = null;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (robot != null) {
			try {
				RobotObjectDetection.stopDetection(robot);
			} catch (RobotException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		super.onDestroy();
		wakeUp = false;
	}

	private MissionApplication getMissionApplication() {
		return (MissionApplication) this.getApplication();
	}

	public void onStopObjectDetect(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					System.out.println("Stop move and detection");
					RobotObjectDetection.stopDetection(robot);
					RobotMotionLocomotionController.moveStop(robot);
				} catch (RobotException e) {
					e.printStackTrace();
				}

			}
		}).start();

	}

	public void onStopMove(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}

		MissionApplication app = getMissionApplication();
		app.setPositionGrass(fromX, fromY);
		app.setPositionGrass(toX, toY);
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {

					System.out.println("Stop move and detection");
					RobotMotionLocomotionController.moveStop(robot);
				} catch (RobotException e) {
					e.printStackTrace();
				}

			}
		}).start();

	}

	private void mission(MissionApplication app, final boolean back) {
		//set láº¡i vá»‹ trÃ­ hiá»‡n táº¡i cá»§a wabo 
		app.setPositionGrass(toX, toY);
		app.setPositionWabo(fromX, fromY);

		log("from X " + fromX + " Y " + fromY + " to X " + toX + " Y" + toY);
		//tÃ¬m Ä‘Æ°á»�ng Ä‘i 
		Path path = app.findPath(fromX, fromY, toX, toY);
		log("path " + (path != null));
		if (path != null) {
			app.resetState(fromX, fromY, toX, toY);
			//tÃ­nh toÃ¡n cÃ¡c khoáº£ng cÃ¡ch hay gÃ³c cáº§n quay Ä‘á»ƒ Ä‘áº¿n Ä‘iá»ƒm Ä‘Ã­ch 
			ArrayList<Step> arr = DimenConvert.optimizeStep(path.getSteps());
			for (Step s : arr) {
				System.out.println("step " + s.getX() + " " + s.getY());
			}

			final ArrayList<RobotMoveTargetPosition> targets = DimenConvert
					.calculateTargetPosition2(arr);
			for (RobotMoveTargetPosition s : targets) {
				System.out.println("target " + s.x + " " + s.y + " " + s.theta);
			}

			new Thread(new Runnable() {

				@Override
				public void run() {
					//di chuyá»ƒn 
					for (RobotMoveTargetPosition s : targets) {
						currentPosition = s;
						String log = "target " + s.x + " " + s.y + " "
								+ s.theta;
						log(log);
						System.out.println("target " + s.x + " " + s.y + " "
								+ s.theta);
						WaboUtils.move(s.x, s.y, s.theta, wakeUp, robot);
						if (!wakeUp) {
							wakeUp = true;
						}
					}

					if (back) {
						//quay robot láº¡i gÃ³c tháº³ng vá»›i trá»¥c x 
						float theta = DimenConvert.currentAngle * -1;
						log("final theta " + theta);
						if (theta != 0.0f) {
							if (theta < -Math.PI) {
								theta = (float) (2 * Math.PI + theta);
							}

							if (theta > Math.PI) {
								theta = (float) (theta - 2 * Math.PI);
							}
							RobotMoveTargetPosition rot = new RobotMoveTargetPosition(
									0, 0, theta);
							WaboUtils.move(rot.x, rot.y, rot.theta, wakeUp,
									robot);
						}
						//dá»«ng láº¡i 
						try {
							RobotMotionLocomotionController.moveStop(robot);
						} catch (RobotException e) {
							e.printStackTrace();
						}
					} else {
						//quay robot hÆ°á»›ng ngÆ°á»£c vá»›i trá»¥c x Ä‘á»ƒ Ä‘i hÆ°á»›ng vÃ o bÃ n á»Ÿ giá»¯a sÃ¢n 
						float theta = DimenConvert.currentAngle * -1;
						log("final theta " + theta);
						if (theta != 0.0f) {
							if (theta < 0) {
								// theta = (float) (2 * Math.PI + theta);
								theta = (float) (Math.PI + theta);
							}

							if (theta > 0) {
								theta = (float) (theta - Math.PI);
							}
							RobotMoveTargetPosition rot = new RobotMoveTargetPosition(
									0, 0, theta);
							WaboUtils.move(rot.x, rot.y, rot.theta, wakeUp,
									robot);

							// kiá»ƒm tra láº¡i vá»‹ trÃ­ hiá»‡n táº¡i 
							// di chuyá»ƒn Ä‘áº¿n Ä‘Ãºng vá»‹ trÃ­ 11,14 vÃ  gÃ³c tháº³ng vÃ o bÃ n 
							try {
								currentPose = WaboSMAC
										.getCurrentPose(getRobot());
								log("current pose "
										+ String.valueOf(currentPose.x) + " "
										+ String.valueOf(currentPose.y) + " "
										+ String.valueOf(currentPose.theta));
								
								moveToPosition(11, 14);
							} catch (final RobotException e) {
								e.printStackTrace();
								makeToast("Get current pose failed! "
										+ e.getMessage());
								return;
							}
						}
						// move until bumper pressed
						try {
							if (!wakeUp) {
								RobotMotionStiffnessController.wakeUp(robot);
								wakeUp = true;
							}
							// báº¯t Ä‘áº§u báº­t monitor kiá»ƒm tra cÃ³ bumbper cháº¡m 
							startSensorsMonitor();
							//Ä‘i cho Ä‘áº¿n khi bumper cháº¡m (náº¿u cháº¡m sáº½ dá»«ng vÃ  Ä‘áº·t quÃ . LÆ°u Ã½ náº¿u dÃ¹ng nÃ³ cÃ³ thá»ƒ pháº£i Ä‘á»ƒ quÃ  trÃªn cao)
							RobotMotionLocomotionController.moveWithVelocity(robot,
							0.2f, 0.0f);
						} catch (RobotException e) {
							e.printStackTrace();
						}
						
					}

				}
			}).start();
		}
	}

	// click events
	/**
	 * di chuyá»ƒn robot tá»« from x from Y Ä‘áº¿n toX toY 
	 * @param v
	 */
	public void onMission(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}
		
		MissionApplication app = getMissionApplication();

		fromX = getSelection(mSpinnerX);
		fromY = getSelection(mSpinnerY);
		// reset current angle
		DimenConvert.currentAngle = 0;
		toX = getSelection(mSpinnerToX);
		toY = getSelection(mSpinnerToY);
		app.setPositionGrass(toX, toY);
		app.setPositionGrass(fromX, fromY);
		mission(app, true);

	}

	public void onRelocate(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}
		relocation();
	}

	public void onMoveBack(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}
		new Thread(new Runnable() {

			@Override
			public void run() {
				WaboUtils.move(-1.0f, 0.0f, 0.0f, wakeUp, robot);
			}
		}).start();
	}

	public void onMovetoTable(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}
		MissionApplication app = getMissionApplication();
		app.setPositionGrass(toX, toY);
		app.setPositionGrass(fromX, fromY);
		//toáº¡ Ä‘á»™ cáº§n Ä‘áº¿n 
		toX = 11;
		toY = 14;
		mission(app, false);

	}

	public void onObjectDetect(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}
		mID = getSelection(mSpinnerObject);
		makeToast("mID "+mID);
		mDetectedObject = false;

		makeToast("Start detection");
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					boolean result = RobotObjectDetection.startDetection(robot);
					System.out.println("Start detection " + result);
					log("Start detection " + result);
					timerDetect = new Timer();
					timerDetect.schedule(new TimerTask() {
						@Override
						public void run() {
							doDetectWhenTimeout();
						}
					}, 4000);
				} catch (RobotException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}).start();

	}

	/**
	 * timeout: rotate robot to check object
	 */
	private void doDetectWhenTimeout() {
		if (!mDetectedObject) {
			System.out.println("Timeout detect");
			try {
				RobotMotionLocomotionController
						.moveWithVelocity(robot, 0, 0.2f);
			} catch (RobotException e) {
				makeToast("move when detect timeout fail: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	public void onArmInit(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}

		new Thread(new Runnable() {

			@Override
			public void run() {
				// return arm to init
				WaboUtils.armInit(robot);

			}
		}).start();
	}

	public void onGetObject(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}
		new Thread(new Runnable() {

			@Override
			public void run() {
				WaboUtils.getObject(robot);
			}
		}).start();

	}

	public void onMoveCloser(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}
		if (mDetectedObject) {
			new Thread(new Runnable() {

				@Override
				public void run() {
					WaboUtils.moveCloserObject(mPose, mID, robot);

				}
			}).start();
		}
	}

	public void onPutObject(View v) {
		if (robot == null) {
			scanRobot();
			return;
		}
		new Thread(new Runnable() {

			@Override
			public void run() {
				WaboUtils.putObject(robot);
			}
		}).start();

	}

	ObjectPose mPose;
	int mID = 2;

	/**
	 * detect marker succeed
	 */
	@Override
	public void onObjectsDetected(final ArrayList<DetectedObject> objects) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				for (DetectedObject obj : objects) {
					ObjectPose pose = obj.getPose();
					int id = obj.getObjectId();
					System.out.println("ID: " + id);
					if (id == mID && !mDetectedObject) {
						mPose = pose;
						mDetectedObject = true;
						timerDetect.cancel();
						System.out.println("ID: " + id + "...." + pose.x + ", "
								+ pose.y + ", " + pose.theta);
						log("ID: " + id + "...." + pose.x + ", " + pose.y
								+ ", " + pose.theta);
						if (robot != null) {
							try {
								System.out.println("Stop move and detection");
								RobotObjectDetection.stopDetection(robot);
								RobotMotionLocomotionController.moveStop(robot);
								// goToObject(pose.x, pose.y, pose.theta, id);
								break;
							} catch (RobotException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}).start();
	}

	private void log(final String log) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				String old = mTvLog.getText().toString();
				if (old == null) {
					old = "";
				}
				old = old + "\n";
				old += log;
				mTvLog.setText(old);
			}
		});
	}

	
	/**
	 * di chuyá»ƒn Ä‘Ãºng vá»‹ trÃ­ cáº§n Ä‘áº¿n dá»±a vÃ o káº¿t quáº£ tá»« hÃ m get current pose. <p>
	 * Sau hÃ m nÃ y sáº½ moveWithVelocity cho Ä‘áº¿n khi cháº¡m bumper
	 * @param x
	 * @param y
	 */
	private void moveToPosition(int x, int y) {
		// convert to unit meter
		final float[] postion = DimenConvert.convert2(x, y);
		log("current position " + String.valueOf(postion[0]) + " "
				+ String.valueOf(postion[1]));

		float pi = (float) Math.PI;
		float currentAngle = currentPose.theta;

		if (currentAngle > 0) {
			currentAngle = (currentAngle - 2 * pi);
		}

		log(" currentAngle " + currentAngle);
		ArrayList<RobotMoveTargetPosition> targets = DimenConvert
				.convert2Target(currentPose.x, currentPose.y, postion[0],
						postion[1], currentAngle);

		for (RobotMoveTargetPosition s : targets) {
			String log = "target " + s.x + " " + s.y + " " + s.theta;
			log(log);
			System.out.println("target " + s.x + " " + s.y + " " + s.theta);
			WaboUtils.move(s.x, s.y, s.theta, wakeUp, robot);
			if (!wakeUp) {
				wakeUp = true;
			}
		}

		float theta = DimenConvert.currentAngle * -1;
		if (theta >= 0) {
			theta = theta - pi;
		} else if (theta < 0) {
			theta = theta + pi;
		}

		log("last theta " + theta);
		WaboUtils.move(0, 0, theta, wakeUp, robot);

	}

	/**
	 * relocate after move back wabo to move to table
	 */
	private void relocation() {
		//hÃ m nÃ y thá»±c hiá»‡n relocate láº¡i vá»‹ trÃ­ cá»§a wabo. 
		//Sau khi relocate xong sáº½ thá»±c hiá»‡n quay robot theo hÆ°á»›ng trá»¥c x cá»§a sÃ¢n (cÃ¡i nÃ y ko cáº§n thiáº¿t láº¯m náº¿u cÃ¡c Ä‘á»™i tá»± tÃ­nh toÃ¡n láº¡i Ä‘Æ°á»£c)
		// Sau Ä‘Ã³ di chuyá»ƒn robot 1 Ä‘iá»ƒm trÃªn sÃ¢n tÃ­nh theo há»‡ trá»¥c sau khi chia sÃ¢n theo cÃ¡c Ä‘Æ°á»�ng cÃ³ khoáº£ng cÃ¡ch 25 cm. 
		new Thread(new Runnable() {
			@Override
			public void run() {
				// relocate
				RobotPose2D resultPose = null;
				try {
					resultPose = WaboSMAC.relocation(robot);
				} catch (final RobotException e) {
					e.printStackTrace();
					makeToast("Relocation failed! " + e.getMessage());
					return;
				}
				// success or not
				if (resultPose == null) {
					makeToast("Relocation failed!");
				} else {
					makeToast("Relocation success! ");
					currentPose = new RobotPose2D();
					currentPose.x = resultPose.x;
					currentPose.y = resultPose.y;
					currentPose.theta = resultPose.theta;
					// log
					log("current pose " + String.valueOf(currentPose.x) + " "
							+ String.valueOf(currentPose.y) + " "
							+ String.valueOf(currentPose.theta));

					// get coordinate in map (0->23)
					int[] pos = DimenConvert.convertInMap(
							(int) (currentPose.x * 100),
							(int) (currentPose.y * 100));

					// get difference between values
					log("current position in coordinate "
							+ String.valueOf(pos[0]) + " "
							+ String.valueOf(pos[1]));

					fromX = pos[0];
					fromY = pos[1];
					// convert to unit meter (toáº¡ Ä‘á»™ lÃ m trÃ²n theo há»‡ toáº¡ Ä‘á»™ mÃ£ hoÃ¡ (Ä‘iá»ƒm Ä‘Ã£ Ä‘Æ°á»£c mÃ£ hoÃ¡) 
					final float[] postion = DimenConvert.convert2(pos[0],
							pos[1]);
					log("current position " + String.valueOf(postion[0]) + " "
							+ String.valueOf(postion[1]));
					new Thread(new Runnable() {

						@Override
						public void run() {
							//quay cho rbot hÆ°á»›ng tháº³ng theo trá»¥c x 
							WaboUtils.move(0.0f, 0.0f,
									(currentPose.theta * -1), wakeUp, robot);

							//tÃ­nh khoáº£ng cÃ¡ch cáº§n Ä‘i Ä‘á»ƒ robot di chuyá»ƒn Ä‘áº¿n vá»‹ trÃ­ gáº§n nháº¥t theo há»‡ toáº¡ Ä‘á»™ mÃ£ hoÃ¡ 
							ArrayList<RobotMoveTargetPosition> targets = DimenConvert
									.convert2Target(currentPose.x,
											currentPose.y, postion[0],
											postion[1]);
							// log("targetMove " + String.valueOf(targetMove.x)
							// + " "
							// +
							// String.valueOf(targetMove.y)+" "+String.valueOf(targetMove.theta));
							// WaboUtils.move(targetMove, wakeUp, robot);

							for (RobotMoveTargetPosition s : targets) {
								String log = "target " + s.x + " " + s.y + " "
										+ s.theta;
								log(log);
								System.out.println("target " + s.x + " " + s.y
										+ " " + s.theta);
								WaboUtils
										.move(s.x, s.y, s.theta, wakeUp, robot);
								if (!wakeUp) {
									wakeUp = true;
								}
							}

							float theta = DimenConvert.currentAngle;

							System.out.println("current angle " + theta);
							// if (theta != 0.0f) {
							// if (theta < 0) {
							// //theta = (float) (2 * Math.PI + theta);
							// theta = (float) (Math.PI + theta);
							// }
							//
							// if (theta > 0) {
							// theta = (float) (theta - Math.PI);
							// }
							// RobotMoveTargetPosition rot = new
							// RobotMoveTargetPosition(
							// 0, 0, theta);
							// WaboUtils.move(rot.x, rot.y, rot.theta, wakeUp,
							// robot);
							// }

							// if (theta != 0.0f) {
							// RobotMoveTargetPosition rot = new
							// RobotMoveTargetPosition(
							// 0, 0, theta);
							// WaboUtils.move(rot.x, rot.y, rot.theta, wakeUp,
							// robot);
							// }

						}
					}).start();
				}
			}
		}).start();
	}

	@Override
	public void onBumperPressed(BumperPosition bumper) {
		if (bumper == BumperPosition.LEFT) {
			makeToast("Left bumper was pressed!");
		} else if (bumper == BumperPosition.CENTER) {
			makeToast("Center bumper was pressed!");
		} else if (bumper == BumperPosition.RIGHT) {
			makeToast("Right bumper was pressed!");
		}

		log("bumper pressed " + bumper.name());
		if (robot == null) {
			return;
		}

		// stop move
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					System.out.println("Stop move and detection");
					RobotMotionLocomotionController.moveStop(robot);
				} catch (RobotException e) {
					e.printStackTrace();
				}

			}
		}).start();
		try {
			stopSensorsMonitor();
		} catch (RobotException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onButtonPressed(ButtonPosition arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCliffDetected(CliffPosition arg0) {
		// TODO Auto-generated method stub

	}

	private boolean startSensorsMonitor() throws RobotException {
		boolean result = mSensorsMonitor.start();
		log("startSensorsMonitor " + result);
		return result;
	}

	private boolean stopSensorsMonitor() throws RobotException {
		boolean result = mSensorsMonitor.stop();
		log("stopSensorsMonitor " + result);
		return result;
	}

	@Override
	public void onProximityChanged(ProximityPosition arg0, ProximityState arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSonarChanged(SonarPosition arg0, double arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onWheelDropped(WheelPosition arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onWheelRaised(WheelPosition arg0) {
		// TODO Auto-generated method stub

	}
}
