package uet.invincible.algorithm;

import java.awt.*;
import java.util.ArrayList;

import android.graphics.Point;
import android.graphics.PointF;

/**
 * Created by Hoang on 23/09/2014.
 */

public class Wabo {
	
    // declare important point
    public static final Point2D.Double startPoint1 = new Point2D.Double() {{setLocation(-2.5, -2.5);}};
    public static final Point2D.Double startPoint2 = new Point2D.Double() {{setLocation(-2.5, 2.5);}};
    public static final Point2D.Double topCenter = new Point2D.Double() {{setLocation(-2, 0);}};
    public static final Point2D.Double rightPoint = new Point2D.Double() {{setLocation(0, 1.5);}};
    public static final Point2D.Double leftPoint = new Point2D.Double() {{setLocation(2, 0);}};
    public static final Point2D.Double bo
    ttomCenter = new Point2D.Double() {{setLocation(0, -1.5);}};
    public static final Point2D.Double bottomLeft = new Point2D.Double() {{setLocation(2.5, -2.5);}};
    public static final Point2D.Double bottomRight = new Point2D.Double() {{setLocation(2.5, 2.5);}};

    // Declare safe zone
    public static final ArrayList<Point2D.Double> safeZone = new ArrayList<Point2D.Double>() {{
        add(startPoint1);
        add(startPoint2);
        add(topCenter);
        add(leftPoint);
        add(rightPoint);
        add(bottomCenter);
    }};

    // declare target point
    public static final Point2D.Double table0 = new Point2D.Double() {{setLocation(-1.5, -1.5);}};
    public static final Point2D.Double table1 = new Point2D.Double() {{setLocation(-1.5, 1.5);}};
    public static final Point2D.Double table2 = new Point2D.Double() {{setLocation(0, 0);}};
    public static final Point2D.Double table3 = new Point2D.Double() {{setLocation(1.5, 1.5);}};
    public static final Point2D.Double table4 = new Point2D.Double() {{setLocation(1.5, -1.5);}};
    public static final Point2D.Double dishesTableLeft = new Point2D.Double() {{ setLocation(2.5, -2);}};
    public static final Point2D.Double dishesTableCenter = new Point2D.Double() {{ setLocation(2.5, 0);}};
    public static final Point2D.Double dishesTableRight = new Point2D.Double() {{ setLocation(2.5, 2);}};

    //add target point to list :(
    public static final ArrayList<Point2D.Double> target = new ArrayList<Point2D.Double>(){{
        add(table0);
        add(table1);
        add(table2);
        add(table3);
        add(table4);
        add(dishesTableLeft);
        add(dishesTableCenter);
        add(dishesTableRight);
    }};

    //set radius for safe zone
    public static final double zoneRadius = 0.5;


    // distance from point a to point b
    public static double distance(Point2D.Double a, Point2D.Double b) {
        double result;
        result = Math.sqrt((a.getX() - b.getX()) * (a.getX() - b.getX()) + (a.getY() - b.getY()) * (a.getY() - b.getY()));
        return result;
    }

    // check if point b inside safe zone a
    public static boolean inside(Point2D.Double a, Point2D.Double b) {
        if( a.getX() + zoneRadius >= b.getX() && a.getY() + zoneRadius >= b.getY() ) {
            return  true;
        }
        return  false;
    }

    //check if target accessible from a zone "A" to target B
    public static boolean accessible(Point2D.Double a, Point2D.Double b) {
        if( a.equals(safeZone.get(0)) ) {
            if( b.equals(target.get(0)) ) {
                return true;
            } return false;
        }
        if( a.equals(safeZone.get(1)) ) {
            if( b.equals(target.get(1)) ) {
                return true;
            } return false;
        }
        if( a.equals(safeZone.get(2)) ) {
            if( b.equals(target.get(0)) || b.equals(target.get(1)) ) {
                return true;
            } return false;
        }
        if( a.equals(safeZone.get(3)) ) {
            if( b.equals(target.get(1)) || b.equals(target.get(2)) ) {
                return true;
            } return false;
        }
        if( a.equals(safeZone.get(4)) ) {
            if( b.equals(target.get(2)) || b.equals(target.get(3)) || b.equals(target.get(4)) ) {
                return true;
            } return false;
        }
        if( a.equals(safeZone.get(5)) ) {
            if( b.equals(target.get(0)) || b.equals(target.get(2)) ) {
                return true;
            } return false;
        }
        return false;
    }

    public ArrayList<Point2D.Double> getPath(Point2D.Double from, Point2D.Double to) {
        ArrayList<Point2D.Double> path = new ArrayList<Point2D.Double>();

        // special fixed path
        if (inside(from, safeZone.get(0))) {
            if ( to.equals(target.get(5)) || to.equals(target.get(6)) || to.equals(target.get(7)) ) {
                path.add(bottomLeft);
                path.add(to);
                return path;
            }
        }

        if (inside(from, safeZone.get(1))) {
            if ( to.equals(target.get(5)) || to.equals(target.get(6)) || to.equals(target.get(7)) ) {
                path.add(bottomRight);
                path.add(to);
                return path;
            }
        }

        // check if it start from one of the safe zone
        int startZone = -1;
        for( int i=0; i<safeZone.size(); i++) {
            if( inside(safeZone.get(i), from) ) {
                startZone = i;
                break;
            }
        }

        // if it a random point on map
        if( startZone == -1 ) {
            double minDistance = 1000000;
            for(int i = 2; i<=safeZone.size(); i++) {
                double curDistance = distance(safeZone.get(i), from);
                // assume the nearest is accessible zone
                if( curDistance < minDistance ) {
                    minDistance = curDistance;
                    startZone = i;
                }
            }
            // move to start zone
            path.add( safeZone.get(startZone) );
        }

        // if start from a zone
        if( !accessible(safeZone.get(startZone), to)) {
            // find the nearest safe zone that accessible to target
            double minDistance = 1000000;
            Point2D.Double nextSafeZone = safeZone.get(startZone);
            for(int i=2; i<=safeZone.size(); i++) {
                if( i!= startZone ) {
                    Point2D.Double cur = safeZone.get(i);
                    double curDistance =  distance(safeZone.get(startZone), cur);
                    if( accessible(cur, to) && curDistance < minDistance) {
                        minDistance = curDistance;
                        nextSafeZone = cur;
                    }
                }
            }
            path.add(nextSafeZone);
        }

        path.add(to);
        return path;
    }
    
    public static void main(String[] args) {
        Wabo newWabo = new Wabo();
        // start point checking
        Point2D.Double start = new Point2D.Double() {{
            setLocation(-2.5, -2.5);
        }};

        // destination point checking
        Point2D.Double to = new Point2D.Double() {{
            setLocation(2.5, 2.5);
        }};
//
//        //check distance
//        double testVar = newWabo.distance(start, to);
//        System.out.printf("%.1f \n", testVar);

        ArrayList<Point2D.Double> path = newWabo.getPath(start, to);
        for (int i=0; i<path.size(); i++) {
            System.out.printf("%f %f \n", path.get(i).getX(), path.get(i).getY());
        }
    }
}
