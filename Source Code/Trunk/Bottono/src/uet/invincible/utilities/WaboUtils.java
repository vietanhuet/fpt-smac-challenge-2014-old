package uet.invincible.utilities;

import com.fpt.robot.Robot;
import com.fpt.robot.RobotException;
import com.fpt.robot.motion.RobotMotionLocomotionController;
import com.fpt.robot.motion.RobotMotionStiffnessController;
import com.fpt.robot.types.RobotMoveTargetPosition;
import com.fpt.robot.vision.RobotObjectDetection.ObjectPose;
import com.fpt.robot.wabo.WaboArm;
import com.fpt.robot.wabo.WaboSMAC;

public class WaboUtils {
	/**
	 * move robot to position (x , y, theta). if robot is not waked up yet, call wakeUp method
	 * @param x
	 * @param y
	 * @param theta
	 * @param wakeUp
	 * @param robot
	 */
	public static void move(final float x, final float y, final float theta,
			final boolean wakeUp, final Robot robot) {

		try {
			//check wakeUp
			if (!wakeUp) {
				RobotMotionStiffnessController.wakeUp(robot);
			}
			//move 
			RobotMoveTargetPosition position = new RobotMoveTargetPosition(x,
					y, theta);
			RobotMotionLocomotionController.moveTo(robot, position);
		} catch (RobotException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * move robot to position (x , y, theta). if robot is not waked up yet, call wakeUp method
	 * @param target
	 * @param wakeUp
	 * @param robot
	 */
	public static void move(RobotMoveTargetPosition target,
			final boolean wakeUp, final Robot robot) {

		move(target.x, target.y, target.theta, wakeUp, robot);
	}
	/**
	 * put object on table 
	 * 1. run gesture: PLACE_DOWN 
	 * 2. detach to Object: set magnetic to false: drop object 
	 * 3. run gesture: RETURN_INIT  
	 * @param robot
	 */
	public static void putObject(final Robot robot) {
		System.out.println("detech object");
		boolean result = false;
		try {
			result = WaboArm.runGesture(robot, "PLACE_DOWN");
			System.out.println("PLACE_DOWN " + result);
			result = WaboSMAC.detachObject(robot);
			System.out.println("detach " + result);
			result = WaboArm.runGesture(robot, "RETURN_INIT");
			System.out.println("RETURN_INIT " + result);
		} catch (RobotException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * get object with flows: (only robot moved close to object) 
	 * 1. run gesture: approach object 
	 * 2. attach to Object: set magnetic to true
	 * 3. run gesture: PICK_UP  
	 * @param robot
	 */
	public static void getObject(final Robot robot) {
		System.out.println("get object");
		boolean result = false;
		try {
			result = WaboArm.runGesture(robot, "APPROACH_OBJ");
			System.out.println("APPROACH_OBJ " + result);
			result = WaboSMAC.attachObject(robot);
			System.out.println("attach " + result);
			result = WaboArm.runGesture(robot, "PICK_UP");
			System.out.println("PICK_UP " + result);
		} catch (RobotException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * move arm to the initialized position   
	 * @param robot
	 */
	public static void armInit(Robot robot) {
		try {
			boolean result = WaboArm.runGesture(robot, "RETURN_INIT");
			System.out.println("RETURN_INIT " + result);
		} catch (RobotException e) {
			e.printStackTrace();
		}
	}
	
	public static void wakeup(Robot robot) {
		try {
			RobotMotionStiffnessController.wakeUp(robot);
		} catch (RobotException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * move to close to object using the current position of object and id of object
	 * @param pose
	 * @param id
	 * @param robot
	 */
	public static void moveCloserObject(ObjectPose pose,int id,Robot robot) {
		float x = pose.x;
		float y = pose.y;
		float theta = pose.theta * -1;
		double distance = Math.sqrt(x * x + y * y);
		try {
			boolean result = false;
			//check distance >0.7. if true --> move until distance < 0.7
			if (distance > 0.6f) {
				System.out.println("move to x-0.7");
				result = RobotMotionLocomotionController.moveTo(robot,
						new RobotMoveTargetPosition(x - 0.7f, 0, 0.0f));
				System.out.println("move to x-0.7 "+result);
			}
			//move to near object
			System.out.println("move closer to object, id: " + id);
			result = WaboSMAC.moveCloserToObject(robot, id);
			System.out.println("moveCloserToObject " + result);
			//move more 10cm
			//result = RobotMotionLocomotionController.moveTo(robot,
			//		new RobotMoveTargetPosition(0.07f, 0, 0.0f));
			
			//result = RobotMotionLocomotionController.moveStop(robot);
			System.out.println("move more  " + result);
		} catch (RobotException e) {
			e.printStackTrace();
		}
	}
}
