package uet.invincible.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import uet.invincible.R;
import uet.invincible.activities.RobotMainActivity;
import uet.invincible.bases.BaseFragment;
import uet.invincible.customize.MyEditText;
import uet.invincible.customize.MyTextView;
import uet.invincible.utilities.WaboUtils;

public class WaboControlerFragment extends BaseFragment {

	private MyEditText x;
	private MyEditText y;
	private MyEditText theta;
	private MyEditText objid;
	
	private MyTextView moveTo;
	private MyTextView stopMove;
	private MyTextView moveSpeed;
	private MyTextView putObj;
	private MyTextView getObj;
	private MyTextView moveCloser;
	private MyTextView armInit;
	private MyTextView weakup;
	
	@Override
	protected void initModels() {
		// init list order
	}

	@Override
	protected void initViews(View view) {
		x = (MyEditText) view.findViewById(R.id.fragment_wabo_controler_ed_x);
		y = (MyEditText) view.findViewById(R.id.fragment_wabo_controler_ed_y);
		theta = (MyEditText) view.findViewById(R.id.fragment_wabo_controler_ed_theta);
		objid = (MyEditText) view.findViewById(R.id.fragment_wabo_controler_ed_closer_object_id);
		moveTo = (MyTextView) view.findViewById(R.id.fragment_wabo_controler_move_to);
		moveTo.setOnClickListener(this);
		moveSpeed = (MyTextView) view.findViewById(R.id.fragment_wabo_controler_move_speed);
		moveSpeed.setOnClickListener(this);
		stopMove = (MyTextView) view.findViewById(R.id.fragment_wabo_controler_stop_move);
		stopMove.setOnClickListener(this);
		putObj = (MyTextView) view.findViewById(R.id.fragment_wabo_controler_putobj);
		putObj.setOnClickListener(this);
		getObj = (MyTextView) view.findViewById(R.id.fragment_wabo_controler_getobj);
		getObj.setOnClickListener(this);
		moveCloser = (MyTextView) view.findViewById(R.id.fragment_wabo_controler_move_closer);
		moveCloser.setOnClickListener(this);
		armInit = (MyTextView) view.findViewById(R.id.fragment_wabo_controler_arm_init);
		armInit.setOnClickListener(this);
		weakup = (MyTextView) view.findViewById(R.id.fragment_wabo_controler_wakeup);
		weakup.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.fragment_wabo_controler_move_to:
			WaboUtils.move(Float.parseFloat(x.getText().toString()),
					Float.parseFloat(y.getText().toString()),
					Float.parseFloat(theta.getText().toString()), true,
					mRobotActivity.getRobot());
			break;
		case R.id.fragment_wabo_controler_move_speed:
			break;
		case R.id.fragment_wabo_controler_stop_move:
			
			break;
		case R.id.fragment_wabo_controler_putobj:
			WaboUtils.putObject(mRobotActivity.getRobot());
			break;
		case R.id.fragment_wabo_controler_getobj:
			WaboUtils.getObject(mRobotActivity.getRobot());
			break;
		case R.id.fragment_wabo_controler_move_closer:
//			WaboUtils.moveCloserObject(pose, objid.getText().toString(), mRobotActivity.getRobot());
			break;
		case R.id.fragment_wabo_controler_arm_init:
			new Thread(new Runnable() {
				@Override
				public void run() {
					WaboUtils.armInit(mRobotActivity.getRobot());
				}
			}).start();
			break;
		case R.id.fragment_wabo_controler_wakeup:
			WaboUtils.wakeup(mRobotActivity.getRobot());
			break;
		default:
			break;
		}
	}
	
	@Override
	protected void initAnimations() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_wabo_controler, container, false);
		initViews(view);
		initModels();
		initAnimations();
		return view;
	}
	private static final long serialVersionUID = -1379377360733503854L;
	private static WaboControlerFragment mInstance;
	public static RobotMainActivity mRobotActivity;
	public static WaboControlerFragment getInstance(RobotMainActivity mRobotActivity2) {
		if (mInstance == null) {
			mInstance = new WaboControlerFragment();
		}
		mRobotActivity = mRobotActivity2;
		return mInstance;
	}

}
